const formButton = document.getElementById('form-toggle');
const productsDiv = document.getElementById('products-div');
const addNew = document.getElementById('submit');
const editItem = document.getElementById('update-submit');

const divMaker = (itemId, itemName, itemPrice, itemImg, itemSummary) => {
    let rootdiv = document.createElement('div');
    rootdiv.setAttribute('class', 'col-sm-4 col-lg-4 col-md-4');
    rootdiv.setAttribute('id', itemId);

    let parentdiv = document.createElement('div');
    parentdiv.setAttribute('class', 'thumbnail');

    let prodImg = document.createElement('img');
    prodImg.setAttribute('src', 'images/products/' +itemImg);
    prodImg.setAttribute('alt', itemName);
    prodImg.setAttribute('data-toggle', 'tooltip');
    prodImg.setAttribute('title', itemId);

    let siblingDivOne = document.createElement('div');
    siblingDivOne.setAttribute('class', 'caption');

    let siblingDivTwo = document.createElement('div');
    siblingDivTwo.setAttribute('class', 'ratings');

    let rightHeader = document.createElement('h4');
    rightHeader.setAttribute('class', 'pull-right')
    rightHeader.textContent = '$' + itemPrice;

    let leftHeader = document.createElement('h4');

    let nameAnchor = document.createElement('a');
    nameAnchor.setAttribute('href', '#')
    nameAnchor.textContent = itemName;

    let paragraphA = document.createElement('p');
    paragraphA.textContent = itemSummary;

    let paragraphB = document.createElement('p');
    paragraphB.setAttribute('class', 'pull-right');

    let paragraphC = document.createElement('p');
   
    let spanA = document.createElement('span');
    spanA.textContent = 'Update Item';
    let spanB = document.createElement('span');
    spanB.setAttribute('class', 'glyphicon glyphicon-edit');
    spanB.setAttribute('data-target', '#updateitem');
    spanB.setAttribute('data-toggle', 'modal');
    spanB.setAttribute('data-targetedit', itemId);


    let button = document.createElement('button');
    button.setAttribute('class', 'btn btn-primary');
    button.setAttribute('data-itemidentifier', itemId);
    button.textContent = 'Remove Item';
    leftHeader.appendChild(nameAnchor);

    paragraphC.appendChild(button);
    paragraphB.appendChild(spanA);
    paragraphB.appendChild(spanB);

    siblingDivTwo.appendChild(paragraphB);
    siblingDivTwo.appendChild(paragraphC);

    siblingDivOne.appendChild(rightHeader);
    siblingDivOne.appendChild(leftHeader);
    siblingDivOne.appendChild(paragraphA);

    parentdiv.appendChild(prodImg);
    parentdiv.appendChild(siblingDivOne);
    parentdiv.appendChild(siblingDivTwo);

    rootdiv.appendChild(parentdiv);

    productsDiv.appendChild(rootdiv);
}

const formToggle = () => {
    const form = document.getElementById('form-div');
    if (form.style.display === "none") {
        form.style.display = "block";
    } else {
        form.style.display = "none";
    }
}
formButton.addEventListener("click", formToggle);

function saveItems() {
    let productname = document.getElementById('item-name').value;
    let price = document.getElementById('item-price').value;
    let image = document.getElementById('item-image').value;
    let description = document.getElementById('item-summary').value;
    let product_id = document.getElementById('item-id').value;
    divMaker(product_id, productname, price, image, description);
    const storeObject = {
        id: product_id,
        name: productname,
        price: price,
        image: image,
        description: description
    }
    window.localStorage.setItem(product_id, JSON.stringify(storeObject));    
}
addNew.addEventListener('click', saveItems);

function displayAll(){
    let database = localStorage
    let accessKeys = Object.keys(database);
    accessKeys.forEach((objectItem) => {
        let entry = JSON.parse(localStorage.getItem(objectItem))
        divMaker(entry.id, entry.name, entry.price, entry.image, entry.description)
    })
}
window.addEventListener('load', displayAll)

function removeItem(event) {
    if(event.target.hasAttribute('data-itemidentifier')) {
        let removeableItem = event.target.dataset.itemidentifier;
        localStorage.removeItem(removeableItem);
        location.reload();
    }
}
window.addEventListener('click', removeItem);

function reform(event) {
    if(event.target.hasAttribute('data-targetedit')){
        let targetKey = event.target.dataset.targetedit;
        let targetItem = JSON.parse(localStorage.getItem(targetKey));
        const targetId = targetItem.id,
            targetName = targetItem.name,
            targetPrice = targetItem.price,
            targetImg = targetItem.image,
            targetDesc = targetItem.description

        // Transfer data to modal form
        document.getElementById('update-item-id').value = targetId;
        document.getElementById('update-item-name').value = targetName;
        document.getElementById('update-item-price').value = targetPrice;
        document.getElementById('update-item-image').value = targetImg;
        document.getElementById('update-item-summary').value = targetDesc;
    }
}
window.addEventListener('click', reform);

function updateItem(){
    let productname = document.getElementById('update-item-name').value;
    let price = document.getElementById('update-item-price').value;
    let image = document.getElementById('update-item-image').value;
    let description = document.getElementById('update-item-summary').value;
    let product_id = document.getElementById('update-item-id').value;
    const updateStoreObject = {
        id: product_id,
        name: productname,
        price: price,
        image: image,
        description: description
    }
    window.localStorage.setItem(product_id, JSON.stringify(updateStoreObject));
    location.reload();
}
editItem.addEventListener('click', updateItem);
