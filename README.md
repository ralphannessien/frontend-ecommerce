# E-Commerce Web

## Introduction

E_Commerce Web is a client-side web application that is only capable of saving data to a JavaScript object. This means that as soon as the browser is refreshed or exited, any data added at run time will not persist.

## How to Use

1. Add new items to the website by clicking on the `New Items Form<` button.
2. Click `Submit` to save data.
3. Delete an item from the list by clicking the `Remove Item` button.
4. Update an item by clicking the update icon on the listed item.
5. Use images in the `products` that is within the `images` folder.(`dress1.png`, `dress2.png`, `dress3.png`... etc.) 